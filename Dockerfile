FROM registry.gitlab.com/digimountain/docker/java:openjdk7

ADD grails-*.zip /opt/grails.zip
RUN cd /opt && unzip grails.zip && rm grails.zip && mv /opt/grails-* /opt/grails
ENV GRAILS_HOME=/opt/grails
ENV PATH=$PATH:$GRAILS_HOME/bin

