#!/bin/bash

DOCKER_IMAGE_TAG=${1:-latest}
DOCKER_IMAGE_REGISTRY=${2:-$(basename "$(pwd)")}

docker build --force-rm -t $DOCKER_IMAGE_REGISTRY:$DOCKER_IMAGE_TAG $(pwd)
